public class SingleLinkedList {

	private Node head;
	private int size;

	public SingleLinkedList() {
		super();
		head = null;
		size = 0;
	}

	public void addToFront(Object data) {
		Node newNode = new Node(data);

		if (head == null) {
			head = newNode;
		} else {
			newNode.setLink(head);
			head = newNode;
		}
	}

	public void addToLast(Object data) {
		Node newNode = new Node(data);

		if (head == null) {
			head = newNode;
		} else {
			Node temp = head;

			while (temp.getLink() != null) {
				temp = temp.getLink();
			}

			temp.setLink(newNode);
		}
		size++;
	}
	
	public void addToMiddle(Object data) {
		Node newNode = new Node(data);

		if (head == null) {
			head = newNode;
		} else if ((int) data < (int) head.getData()) {
			newNode.setLink(head);
			head = newNode;
		} else { 
			Node temp = head;
			Node prev = null;
			
			while (temp != null && (int) data > (int) temp.getData()) {
				prev = temp;
				temp = temp.getLink();
			}
			
			newNode.setLink(temp);
			prev.setLink(newNode);
		}
		size++;
	}

	public void remove(Object data) {
		if (head == null) {
			System.err.println("Error: There is no data");
		} else {
			while ((int) data == (int) head.getData()) {
				head = head.getLink();
				size--;
			}
			
			Node temp = head;
			Node prev = null;
			
			while (temp != null) {
				if ((int) data == (int) temp.getData()) {
					prev.setLink(temp.getLink());
					temp = prev;
					size--;
				}
				
				prev = temp;
				temp = temp.getLink();
			}
			
		}
		
	}
	
	public void display() {
		Node temp = head;
		
		while (temp != null) {
			System.out.println((int) temp.getData() + " - ");
			temp = temp.getLink();
		}
	}

	public double findAverage() {
		double average = 0;
		
		Node temp = head;
		
		while (temp != null) {
			average += (int) temp.getData();
			temp = temp.getLink();
		}
		
		return average / size;
	}
	
	public double findVariance() {
		double variance = 0;
		double average = findAverage();
		
		Node temp = head;
		
		while (temp != null) {
			variance += Math.pow((int) temp.getData() - average, 2);
			temp = temp.getLink();
		}
		
		return variance / size;
	}

	public int getSize() {
		return size;
	}

}